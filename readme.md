run command:
```
vsce package

code-server --uninstall-extension $(code-server --list-extensions | grep chameleon)

scp chameleon-vscode-0.1.3.vsix research:/home/ubuntu/

code-server --install-extension chameleon-vscode-0.1.3.vsix

chmod +x ./.local/share/code-server/extensions/chameleon-team.chameleon-vscode-0.1.3/bin/chameleon
```
