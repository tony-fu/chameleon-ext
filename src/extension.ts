import * as vscode from 'vscode';
import * as path from 'path';
import * as os from 'os';
import { spawnSync } from 'child_process';
const Graph = require('graphology');
import './chameleonTypes';
import { allSimpleEdgePaths } from 'graphology-simple-path';
import * as R from 'ramda';
import * as chroma from 'chroma-js';
// import * as R from 'ramda';

const colors = [
  { name: 'blue', code: '#ffd6a5' },
  { name: 'purple', code: '#d1c5ec' },
  { name: 'red', code: '#efbfc1' },
  { name: 'cyan', code: '#4ee3d4' },
];

const colorMuted = "#C0C0C0"

const decorationsArchive: vscode.TextEditorDecorationType[] = [];


const onSave = vscode.workspace.onDidSaveTextDocument((document: vscode.TextDocument) => {
  vscode.commands.executeCommand('chameleon.typecheck')
});


const onSwap = vscode.window.onDidChangeVisibleTextEditors(() => {
  vscode.commands.executeCommand('chameleon.typecheck')
})


type getSubTypes = (t: Type) => string[];
const getSubTypes: getSubTypes = (t) => {
  switch (t.class) {
    case 'con':
      return []
    case 'var':
      return ['var:' + t.name]
    case 'app':
    default:
      return [...getSubTypes(t.type1), ...getSubTypes(t.type2)]
  }
}

type typeName = (t: Type, prefix: string) => string;
const typeName: typeName = (t, prefix) => {
  if (t.class === "con") {
    return 'con:' + prefix + ':' + t.name
  } else if (t.class === "var") {
    return 'var:' + t.name
  } else {
    return 'app:' + prefix
  }
}

export function activate(context: vscode.ExtensionContext) {
  const panel = vscode.window.createWebviewPanel(
    'Types',
    'Chameleon Debugger',
    vscode.ViewColumn.Two,
    { enableScripts: true },
  );

  panel.webview.onDidReceiveMessage(
    message => {
      if (message.command === 'highlight') {
        clearDecorations();
        let pt: Place[] = context.workspaceState.get('placetable') || [];
        let allhighlights: number[] = context.workspaceState.get('allhighlights') || [];
        decrateLocations(
          vscode.window.visibleTextEditors[0],
          pt,
          allhighlights,
          colorMuted,
          ""
        );

        decrateLocations(
          vscode.window.visibleTextEditors[0],
          pt,
          message.locations,
          message.bcolor,
          message.fcolor
        );
      } else if (message.command === 'refresh') {
        clearDecorations();
        let pt: Place[] = context.workspaceState.get('placetable') || [];
        let allhighlights: number[] = context.workspaceState.get('allhighlights') || [];
        decrateLocations(
          vscode.window.visibleTextEditors[0],
          pt,
          allhighlights,
          colorMuted,
          ""
        );
      }
    },
    undefined,
    context.subscriptions,
  );

  context.subscriptions.push(
    vscode.commands.registerCommand('chameleon.typecheck', () => {
      clearDecorations();
      let result = typecheck(
        vscode.window.visibleTextEditors[0].document,
        context.extensionUri.fsPath,
      );

      if (result) {
        let places = result.placetable;
        let allHighlights = Array.from(new Set(
          [...result.decls.flatMap(dec => (dec.mismatch?.allConstraints ?? []).flatMap(c => c.just)),
           ...result.decls.flatMap(dec => (dec.mismatch ?.typeVariants ?? []).flatMap(c => c.locs))]
        ))
        // let allHighlights = Array.from(
        //   new Set(
        //     result.decls
        //       .map(dec => dec.mismatch ? dec.mismatch.typeVariants.map(x => x.locs).flat() : [])
        //       .flat()
        //   )
        // )
        let newDecl: Decl[] = result.decls
          .map((decl: Decl) => {
            if (decl.mismatch !== null) {
              const graph = new Graph();
              const allCons = decl.mismatch.allConstraints;
              allCons.forEach((constraint, index) => {
                const left = typeName(constraint.left, index + ':left');
                const right = typeName(constraint.right, index + ':right');
                graph.mergeNode(left)
                graph.mergeNode(right)
                if (constraint.left.class === 'app') {
                  let newNodes: string[] = getSubTypes(constraint.left);
                  newNodes.forEach(node => {
                    graph.mergeNode(node)
                    graph.mergeUndirectedEdge(left, node, { type: "structure" })
                  })
                }

                if (constraint.right.class === 'app') {
                  let newNodes: string[] = getSubTypes(constraint.right);
                  newNodes.forEach(node => {
                    graph.mergeNode(node)
                    graph.mergeUndirectedEdge(right, node, { type: "structure" })
                  })
                }

                graph.mergeUndirectedEdge(left, right, { type: "constraints", justification: constraint.just })
              });

              let lowestDegrees: string[] = graph.nodes().reduce((accumulator: string[], currentValue: string) => {
                if (accumulator.length === 0) {
                  return [currentValue]
                } else {
                  let currentMinimal = graph.degree(accumulator[0]);
                  let newMinimal = graph.degree(currentValue);
                  if (newMinimal > currentMinimal) {
                    return accumulator
                  } else if (newMinimal < currentMinimal) {
                    return [currentValue]
                  } else {
                    return [...accumulator, currentValue]
                  }
                }
              }, [])
              let paris = lowestDegrees.map((node1: string, i1: number) => {
                return lowestDegrees.filter((_, i2: number) => {
                  return i2 > i1
                })
                  .map(node => {
                    return [node1, node]
                  })
              }).flat()
              let chainLocs: number[][] = paris
              .map(pair => allSimpleEdgePaths(graph, pair[1], pair[0]))
              .flat()
              .reduce((accu, curr) => curr.length > accu.length ? curr : accu, [])
              .filter(edge => graph.getEdgeAttribute(edge, 'type') === 'constraints')
              .map(edge => graph.getEdgeAttribute(edge, 'justification'));

              let paths: ChameleonRange[][] = paris
                .map(pair => allSimpleEdgePaths(graph, pair[1], pair[0]))
                .flat()
                .reduce((accu, curr) => curr.length > accu.length ? curr : accu, [])
                .filter(edge => graph.getEdgeAttribute(edge, 'type') === 'constraints')
                .map(edge => graph.getEdgeAttribute(edge, 'justification'))
                .map(locs => locs
                  .map((l: number) => places.find(x => x.getLoc === l)?.getRange)
                  .filter((x: undefined | ChameleonRange) => x !== undefined))

              let minRow = paths.flat().reduce((accu, curr) => curr.fromRow < accu ? curr.fromRow : accu, Infinity)
              let minCol = 0;
              let maxRow = paths.flat().reduce((accu, curr) => curr.toRow > accu ? curr.toRow : accu, 0)
              let maxCol = paths.flat().reduce((accu, curr) => curr.toCol > accu ? curr.toCol : accu, 0)
              return { ...decl, snapshotSize: [minRow, minCol, maxRow, maxCol], chain: collapse(paths), chainLocs:collapse(chainLocs) }
            } else {
              return decl
            }
          });

        context.workspaceState.update('placetable', result.placetable)
        context.workspaceState.update('allhighlights', allHighlights);

        panel.webview.html = getWebviewContent(
          context.extensionUri,
          panel.webview,
          { 
            ...result,
             decls: newDecl,
          },
        );

        decrateLocations(
          vscode.window.visibleTextEditors[0],
          result.placetable,
          allHighlights,
          colorMuted,
          "black"
        );

      }
    }),
    onSave,
    onSwap,
  );
  vscode.commands.executeCommand('chameleon.typecheck')
}


type typecheck = {
  (document: vscode.TextDocument, extentionPath: string):
    | ChameleonResult
    | undefined;
};
const typecheck: typecheck = function (document, extentionPath) {
  const workPaths = vscode.workspace.workspaceFolders;
  if (workPaths?.length) {
    const workPath = workPaths[0].uri.fsPath;
    const filePath = document.uri.path;
    let fileName: string;
    if (document.uri.scheme === 'file') {
      fileName = filePath
        .split('/')
        .reduce(
          (accu, curr, currIn, arr) =>
            currIn === arr.length - 1 ? accu + curr : accu,
          '',
        );
    } else if (document.uri.scheme === 'git') {
      fileName = filePath
        .split('/')
        .reduce(
          (accu, curr, currIn, arr) =>
            currIn === arr.length - 1 ? accu + curr : accu,
          '',
        )
        .split('.')
        .reduce(
          (accu: string[], curr, currIn, arr) =>
            currIn === arr.length - 1 ? accu : [...accu, curr],
          [],
        )
        .join('.');
    } else {
      fileName = 'none';
    }

    const exePath = path.join(
      extentionPath,
      'bin',
      os.type() === 'Windows_NT' ? 'chameleon.exe' : 'chameleon',
    );

    const chameleonProcess = spawnSync(
      exePath + ` --lib=${path.join(extentionPath, 'bin')} ` + fileName,
      {
        shell: os.type() === 'Windows_NT' ? 'powershell.exe' : '/bin/bash',
        cwd: workPath,
        encoding: 'utf8',
      },
    );
    // if (chameleonProcess.stderr) {
    //   console.log(chameleonProcess.stderr)
    // }
    // console.log(chameleonProcess.stdout)
    let result: ChameleonResult = JSON.parse(chameleonProcess.stdout);
    return result;
  }
};

type viewChameleonResult = (r: ChameleonResult) => string;
const viewChameleonResult: viewChameleonResult = r => {

  let declares = r.decls;
  let declareCopy = [...declares];
  declareCopy.sort(
    (d1, d2) => d1.declInfo.declaredLoc - d2.declInfo.declaredLoc,
  );
  return `
    ${declareCopy
      .filter(d => d.mismatch !== null)
      .map(d => viewDeclartion(d, r.placetable))
      .join('\n')}
  `;
};

type viewDeclartion = (decl: Decl, pt: Place[]) => string;
const viewDeclartion: viewDeclartion = (decl, pt) => {
  const snapshot = 
    !decl.snapshotSize
    || !decl.chain  
    || !decl.chainLocs ? '' : drawSnapshots(decl.snapshotSize, decl.chain, decl.chainLocs, pt)

  return `
    <section>
      <div class="text-lg my-4 text-gray-500">
      <pre style='display:inline' class="text-black">${decl.declInfo.nameOriginal
    }</pre>  can only be one of the following types:
      </div>

      ${decl.mismatch === null
      ? ''
      : viewMismatch(
        decl.declInfo.nameOriginal, 
        decl.mismatch, 
        pt, 
        snapshot
      )
    }
     
    </section>
  `;

};

type inRanges = (a: number, b: number, ranges: ChameleonRange[]) => Boolean;
const inRanges: inRanges = (a, b, ranges) => {
  return ranges.map(({ fromRow, toRow, fromCol, toCol }) => {
    let inR = a >= fromRow && a <= toRow && b >= fromCol && b <= toCol
    return inR
  }).some(x => x === true)
}

type drawSnapshots = (size: [number, number, number, number], chain: ChameleonRange[][], chainLocs: number[][], pt: Place[]) => string;
const drawSnapshots: drawSnapshots = (size, chain,chainLocs, pt) => {
  const rows = size[2] - size[0] + 1;
  const cols = size[3] - size[1] + 1;
  return chainLocs.map((locs, i) => ({ index: i, locs: locs, ranges: chain[i]}))
    .map(c => ({...c, locs: c.locs.filter(x => x >0 )}) )
    .filter(c => c.locs.length !== 0)
    .map (({index, ranges, locs }) => {
      let matrix = 
        Array.from({ length: rows }).map((_, rn) => {
          return Array.from({ length: cols }).map((_, cn) => {
            let inrange = inRanges(rn + size[0], cn, ranges);
            return {inrange, first: cn ===0, last: cn === cols - 1}
          })
        })
        .flat()
      return ({index, ranges, locs, matrix })
    })
    .filter((c, ind, arr) => {
      if (ind === 0) return true
      if (arrayEq(c.locs, arr[ind - 1].locs)) return false
      if (arrayIso(pt, c.locs, arr[ind - 1].locs)) return false
      if (c.matrix.every(({inrange}, i) => {
        let thisInrange = inrange;
        let prevInrange = arr[ind - 1].matrix[i].inrange;
        return thisInrange ? prevInrange : true;
      }))  return false
      return true
    })
    .map(({locs, matrix}, ind, arr) => {
      let c = chroma.scale([colors[0].code,colors[1].code]).mode('lch').colors(arr.length)[ind]
      // #30364d #3c3d41
      return `
      <div 
        class="deduction-grid cursor-pointer rounded-lg"
        data-fcolor="#ffffff"
        data-bcolor="#0073ef"
        data-hover="#30364d"
        data-normal=${c}
        style="--normal-color:${c};--hover-color:#30364d;--hl-color:#0073ef;--row-number:${rows};--col-number:${cols}" 
        data-loc="${locs.toString()}">
        ${matrix.map(({inrange, first, last}, ind, arr) => {
          let firstInrange =  (inrange && first) || (inrange && !arr[ind - 1].inrange) 
          let lastInrange =  (inrange && last) || (inrange && !arr[ind + 1].inrange) 
          return `<span
            class="deduction-col 
            ${inrange ? 'relevent' : 'inrelevent'} 
            ${firstInrange ? 'rounded-l': ''} 
            ${lastInrange? 'rounded-r':''}"> </span>`
        }).join('\n')}
      </div>
      `
    }).join('\n')
}

type viewMismatch = (symbal: string, msm: Mismatch, pt: Place[], inbetween:string) => string;
const viewMismatch: viewMismatch = (s, msm, pt, inbetween) => {
  const types = msm.typeVariants
    .map((v, i) => {
      return `
            <div class="type-variant" 
              data-loc="${v.locs.toString()}" 
              data-fcolor="#ffffff"
              data-bcolor="#0073ef" 
              >
              <pre class="type-variant-sig" style="background-color:#318cef;color:#fff">${s} :: ${v.signature
        }</pre>
            </div>`;
    })
    // .join('\n');
  let [head, ...rest] = types
  return [head, inbetween, ...rest].join('');
};

type getWebviewContent = (
  uri: vscode.Uri,
  webview: vscode.Webview,
  result: ChameleonResult,
) => string;
const getWebviewContent: getWebviewContent = (uri, webview, result) => {
  const stylesPathMainPath = vscode.Uri.joinPath(uri, 'assets', 'vscode.css');
  const tailwindPath = vscode.Uri.joinPath(uri, 'assets', 'tailwind.css');
  const scriptPathMainPath = vscode.Uri.joinPath(uri, 'assets', 'vscode.js');
  const logRocketPath = vscode.Uri.joinPath(uri, 'assets', 'logrocket.js');

  const stylesMainUri = webview.asWebviewUri(stylesPathMainPath);
  const tailwindUri = webview.asWebviewUri(tailwindPath);
  const scriptMainUri = webview.asWebviewUri(scriptPathMainPath);
  const logRocketUri = webview.asWebviewUri(logRocketPath);
  return `<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="${stylesMainUri}" rel="stylesheet">
		<link href="${tailwindUri}" rel="stylesheet">
	</head>
	<body>
    ${viewChameleonResult(result)}
	</body>
  <script src="${logRocketUri}"></script>
  <script src="${scriptMainUri}"></script>
	</html>`;
};

type clearDecorations = () => void;
const clearDecorations: clearDecorations = function () {
  decorationsArchive.forEach((dec: vscode.TextEditorDecorationType) =>
    dec.dispose(),
  );
};

type decrateLocations = (
  editor: vscode.TextEditor,
  placetable: Place[],
  loc: number[],
  bcolor: string,
  fcolor: string,
) => void;
const decrateLocations: decrateLocations = (e, pt, ls, bc, fc) => {
  const errorDeco = vscode.window.createTextEditorDecorationType({
    backgroundColor: bc,
    borderRadius: "4px",
    color:  fc === '' ? undefined:fc
  });

  decorationsArchive.push(errorDeco);
  const ranges = ls.map(l => {
    const place = pt.find(p => p.getLoc === l) ?? {
      getRange: { fromRow: 0, fromCol: 0, toRow: 0, toCol: 0 },
    };
    return new vscode.Range(
      place.getRange.fromRow,
      place.getRange.fromCol,
      place.getRange.toRow,
      place.getRange.toCol,
    );
  });
  e.setDecorations(errorDeco, ranges);
};

function arrayEq(a: number[], b: number[]): boolean {
  let cloneA = R.clone(a);
  let cloneB = R.clone(b);
  cloneA.sort();
  cloneB.sort();
  return R.equals(a, b)
}

function arrayIso(pt: Place[], a: number[], b: number[]) : boolean {
  let cloneA = R.clone(a);
  let cloneB = R.clone(b);
  cloneA.sort();
  cloneB.sort();
  return R.equals(a.map(n => pt.find(p => p.getLoc == n)?.getRange), b.map(n => pt.find(p => p.getLoc == n)?.getRange))
}

function collapse <T> (items: T[][]):T[][] {
  if (items.length ===0) return [];
  if (items.length === 1) return items;
  return Array.from({length: items.length -1}).map((_, index) => {
    return [...items[index],  ...items[index + 1]]
  })
}