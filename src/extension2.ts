import * as vscode from 'vscode'
import * as path from 'path';
import * as os from "os";
import * as chroma from 'chroma-js';
import { spawnSync } from 'child_process';
import "./chameleonTypes"

let decorationsArchive: vscode.TextEditorDecorationType[] = [];


export function activate(c: vscode.ExtensionContext) {
  c.subscriptions.push(
    vscode.commands.registerCommand('chameleon.typecheck', () => {
      clearDecorations()
      let result = vscode.window.activeTextEditor
        ? typecheck(vscode.window.activeTextEditor?.document, c.extensionUri.fsPath)
        : undefined;
      if (vscode.window.activeTextEditor && result) {
        decrateLocations(vscode.window.activeTextEditor, result);
      }
    })
  );
}

type Typecheck = { (document: vscode.TextDocument, extentionPath: string): ChameleonResult | undefined }
const typecheck: Typecheck = function (document, extentionPath) {
  const workPaths = vscode.workspace.workspaceFolders;
  if (workPaths?.length) {
    const workPath = workPaths[0].uri.fsPath;
    const filePath = document.uri.path;
    let fileName: string;
    if (document.uri.scheme === "file") {
      fileName = filePath
        .split('/')
        .reduce((accu, curr, currIn, arr) => currIn === arr.length - 1 ? accu + curr : accu, "");
    } else if (document.uri.scheme === "git") {
      fileName = filePath
        .split('/')
        .reduce((accu, curr, currIn, arr) => currIn === arr.length - 1 ? accu + curr : accu, "")
        .split('.')
        .reduce((accu: string[], curr, currIn, arr) => currIn === arr.length - 1 ? accu : [...accu, curr], [])
        .join('.');
    } else {
      fileName = "none";
    }

    const exePath = path.join(extentionPath, 'bin', os.type() === "Windows_NT" ? 'chameleon.exe' : 'chameleon')
    const chameleonProcess = spawnSync(
      exePath
      + ` --lib=${path.join(extentionPath, 'bin')} `
      + fileName,
      {
        shell: os.type() === "Windows_NT" ? "powershell.exe" : "/bin/bash",
        cwd: workPath, encoding: 'utf8'
      }
    );
    // console.log(chameleonProcess.stderr)

    // console.log(chameleonProcess.stdout)
    let result: ChameleonResult = JSON.parse(chameleonProcess.stdout);
    return result;
  }
};

const clearDecorations = function () {
  decorationsArchive.forEach((dec: vscode.TextEditorDecorationType) => dec.dispose())
};



type DecrateLocations = (editor: vscode.TextEditor, result: ChameleonResult) => void
const decrateLocations: DecrateLocations = (e, res) => {
  let placetable = res.placetable;
  let decls = res.decls;
  let errorDecls = decls.filter(d => d.mismatch !== null)
  console.log(placetable)



  errorDecls.forEach((d, i) => {
    console.log(d.mismatch)
    let variants = d.mismatch?.typeVariants;
    new Set()
    let univ: number[] | undefined = undefined;
    if (d.mismatch) {
      let variants = d.mismatch.typeVariants;
      let allLocations = Array.from(new Set(variants.map(v => v.locs).flat()))
      univ = allLocations.filter(l => variants.map(v => v.locs).every(c => c.includes(l)))
      console.log(univ)
    }

    let declareLoc = d.declInfo.declaredLoc;
    let declareName = d.declInfo.nameOriginal;
    const deco = vscode.window.createTextEditorDecorationType({
      backgroundColor: 'red',
      textDecoration: "none;"
    });
    let place = placetable.find(p => p.getLoc === declareLoc)
    let ran = place === undefined ? undefined : new vscode.Range(place.getRange.fromRow, place.getRange.fromCol, place.getRange.toRow, place.getRange.toCol)
    
    let message = new vscode.MarkdownString(`Possible type mismatch at \`${declareName}\`\n\n`, true)
      .appendMarkdown("It can be\n\n")
      .appendMarkdown('```a -> a``` [why](command:chameleon.typecheck) \n\n')
      .appendMarkdown("Or\n\n")
      .appendMarkdown('```a -> a``` [why](command:chameleon.typecheck) \n\n')

    message.isTrusted = true;
    if (ran) e.setDecorations(deco, [
      {
        range: ran,
        hoverMessage: message
      }])


    let colors = chroma.scale(['#fafa6e', '#2A4858']).mode('lch').colors(2)
    d.mismatch?.typeVariants.forEach((t, j) => {
      const deco = vscode.window.createTextEditorDecorationType({
        backgroundColor: colors[j],
      });
      let ranges = t.locs.filter(l => univ ? !univ.includes(l) : true).map(l => placetable.map(p => p.getLoc === l ? [p] : [])).flat().flat().map(p => {
        return new vscode.Range(p.getRange.fromRow, p.getRange.fromCol, p.getRange.toRow, p.getRange.toCol)
      })
      e.setDecorations(deco, ranges)
    })
  })
}
