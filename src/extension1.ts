import * as vscode from 'vscode'
import * as path from 'path';
import * as os from "os";
import * as chroma from 'chroma-js';
import * as R from 'ramda';
import { spawnSync } from 'child_process';
import "./chameleonTypes"

let decorationsArchive: vscode.TextEditorDecorationType[] = [];
// let alwaysShowDecoration: vscode.TextEditorDecorationType[] = [];

export function activate(c: vscode.ExtensionContext) {
  c.subscriptions.push(
    vscode.commands.registerCommand('chameleon.typecheck', () => {
      // clearHighlightTemrs()
      clearDecorations()
      let result = vscode.window.activeTextEditor
        ? typecheck(vscode.window.activeTextEditor?.document, c.extensionUri.fsPath)
        : undefined;
      
      const panel = vscode.window.createWebviewPanel(
        "Types",
        "Chameleon Debugger",
        vscode.ViewColumn.Two,
        { enableScripts: true }
      )
      if (result) {
        panel.webview.html = getWebviewContent(c.extensionUri, panel.webview, result);
        panel.webview.onDidReceiveMessage(
          message => {
            if (result) {
              clearDecorations()
              decrateLocations(vscode.window.visibleTextEditors[0], result.placetable, message.locations)
            }
          },
          undefined,
          c.subscriptions
        );
      }
    })
  );
}

type Typecheck = { (document: vscode.TextDocument, extentionPath: string): ChameleonResult | undefined }
const typecheck: Typecheck = function (document, extentionPath) {
  const workPaths = vscode.workspace.workspaceFolders;
  if (workPaths?.length) {
    const workPath = workPaths[0].uri.fsPath;
    const filePath = document.uri.path;
    let fileName: string;
    if (document.uri.scheme === "file") {
      fileName = filePath
        .split('/')
        .reduce((accu, curr, currIn, arr) => currIn === arr.length - 1 ? accu + curr : accu, "");
    } else if (document.uri.scheme === "git") {
      fileName = filePath
        .split('/')
        .reduce((accu, curr, currIn, arr) => currIn === arr.length - 1 ? accu + curr : accu, "")
        .split('.')
        .reduce((accu: string[], curr, currIn, arr) => currIn === arr.length - 1 ? accu : [...accu, curr], [])
        .join('.');
    } else {
      fileName = "none";
    }

    const exePath = path.join(extentionPath, 'bin', os.type() === "Windows_NT" ? 'chameleon.exe' : 'chameleon')
    const chameleonProcess = spawnSync(
      exePath
      + ` --lib=${path.join(extentionPath, 'bin')} `
      + fileName,
      {
        shell: os.type() === "Windows_NT" ? "powershell.exe" : "/bin/bash",
        cwd: workPath, encoding: 'utf8'
      }
    );
    console.log(chameleonProcess.stderr)

    console.log(chameleonProcess.stdout)
    let result: ChameleonResult = JSON.parse(chameleonProcess.stdout);
    return result;
  }
};

type ViewChameleonResult = (r: ChameleonResult) => string
const viewChameleonResult: ViewChameleonResult = (r) => {
  let declares = r.decls;
  let declareCopy = [...declares];
  declareCopy.sort((d1, d2) => d1.declInfo.declaredLoc - d2.declInfo.declaredLoc)
  return `
    ${declareCopy.map(d => viewDeclartion(d, r.placetable)).join('\n')}
  `
}

type ViewDeclaration = (decl: Decl, pt: Place[]) => string
const viewDeclartion: ViewDeclaration = (decl, pt) => {
  const typecheckStstus = `<span> ${decl.typeConflict === null ? "✅" : "❌"}</span>`
  return `
    <section>
      <div style="height: 25px">
       ${typecheckStstus} <pre style='display:inline'>${decl.declInfo.nameOriginal}</pre>
      </div>
      
      ${decl.typeConflict === null ? '' : 'Highlighted area contains type mismatch<hr>'}
      
      ${decl.typeConflict === null ? '' : viewTypeConflict(decl.typeConflict, pt)}
    </section>
  `
}


type ViewTypeConflict = (cfl: TypeConflict, pt: Place[]) => string
const viewTypeConflict: ViewTypeConflict = (cfl, pt) => {
  const cs = R.pipe(
    removeNegative,
    removeEmpty,
    removeUniversal,
    removeEmpty,
    (cs: Constraint[]) => removeGhostloc(cs, pt),
    removeEmpty,
    removeDupe,
    (cs: Constraint[]) => removeIso(cs, pt),
  )(cfl.constraints)

  const buttons = cs.map((cons,ind) => {
    return `
      <button class="cons-btn" data-loc="${cons.just.toString()}"></button>
      ${ind !== cs.length - 1 ? '<div class="connector"></div>' : ''}
    `
  }).join('\n')
  let useMinimalType = cfl.typeLeftFull === null || cfl.typeRightFull === null; 
  let logicalTypeL = useMinimalType? cfl.typeLeftMinimal : cfl.typeLeftFull
  let logicalTypeR = useMinimalType? cfl.typeRightMinimal : cfl.typeRightFull
  return `
    <div>
      <div class="type-ctn">
        <span>Possible type 1:<pre>${logicalTypeL}</pre></span>
        <span>Possible type 2:<pre>${logicalTypeR}</pre></span>
      </div>
      <button class="toggle-all" data-loc="${cfl.constraints.map(c => c.just).flat()}">Toggle all highlight</button>
      <div class="cons-btn-ctn">${buttons}</div>

    </div>
  `
}

type RemoveNegative = (cs: Constraint[]) => Constraint[]
const removeNegative: RemoveNegative = cs => cs.map(c => ({ ...c, just: c.just.filter(n => n > 0) }))

type RemoveEmpty = (cs: Constraint[]) => Constraint[]
const removeEmpty: RemoveEmpty = cs => cs.filter(c => c.just.length > 0)

type RemoveGhostloc = (cs: Constraint[], pt: Place[]) => Constraint[]
const removeGhostloc: RemoveGhostloc = (cs, pt) => {
  const locationInPlaceTable = (loc: number) => { 
    let found = pt.find(p => p.getLoc === loc) 
    return found !== undefined && (found.getRange.fromCol !== found.getRange.toCol)
  }
  return cs.map(c => ({ ...c, just: c.just.filter(locationInPlaceTable) }))
}

type RemoveUniversal = (cs: Constraint[]) => Constraint[]
const removeUniversal: RemoveUniversal = (cs) => {
  const locs = Array.from(new Set(cs.map(c => c.just).flat()))
  const univ = locs.filter(l => cs.map(c => c.just).every(c => c.includes(l)))
  return cs.map(c => ({ ...c, just: c.just.filter(l => !univ.includes(l)) }))
}

type RemoveDupe = (cs: Constraint[]) => Constraint[]
const removeDupe: RemoveDupe = (cs) => {
  let uniqes = cs.filter((c, ind, arr) => {
    if (ind === 0) return true
    if (arrayEq(c.just, arr[ind - 1].just)) return false
    return true
  })
  return uniqes
}

type RemoveIso = (cs: Constraint[], pt: Place[]) => Constraint[]
const removeIso: RemoveIso = (cs, pt) => {
  let uniqes = cs.filter((c, ind, arr) => {
    if (ind === 0) return true
    if (arrayIso(pt, c.just, arr[ind - 1].just)) return false
    return true
  })
  return uniqes
}


function getWebviewContent(uri: vscode.Uri, webview: vscode.Webview, r: ChameleonResult) {
  const stylesPathMainPath = vscode.Uri.joinPath(uri, 'assets', 'vscode.css');
  const scriptPathMainPath = vscode.Uri.joinPath(uri, 'assets', 'vscode.js');

  const stylesMainUri = webview.asWebviewUri(stylesPathMainPath);
  const scriptMainUri = webview.asWebviewUri(scriptPathMainPath);

  return `<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="${stylesMainUri}" rel="stylesheet">
	</head>
	<body>
    ${viewChameleonResult(r)}
	</body>
  <script src="${scriptMainUri}"></script>
	</html>`
}

const clearDecorations = function () {
  decorationsArchive.forEach((dec: vscode.TextEditorDecorationType) => dec.dispose())
};


type DecrateLocations = (editor: vscode.TextEditor, placetable: Place[], loc: number[]) => void
const decrateLocations: DecrateLocations = (e, pt, ls) => {
  const errorDeco = vscode.window.createTextEditorDecorationType({
    backgroundColor: "khaki"
  });
  decorationsArchive.push(errorDeco);
  const ranges = ls.map(l => {
    const place = pt.find(p => p.getLoc === l) ?? { getRange: { fromRow: 0, fromCol: 0, toRow: 0, toCol: 0 } }
    return new vscode.Range(place.getRange.fromRow, place.getRange.fromCol, place.getRange.toRow, place.getRange.toCol)
  })
  e.setDecorations(errorDeco, ranges);
}


// const clearHighlightTemrs = function () {
//   alwaysShowDecoration.forEach((dec: vscode.TextEditorDecorationType) => dec.dispose())
// };

// type DecrateTerms = (editor: vscode.TextEditor, placetable: Place[], tc: TypeConflict) => void
// const decrateTemrs: DecrateTerms = (e, pt, tc:TypeConflict) => {
//   const errorDeco = vscode.window.createTextEditorDecorationType({
//     backgroundColor: "pink",
//   });
//   alwaysShowDecoration.push(errorDeco);
//   const place = pt.find(p => p.getLoc === tc.conflictTerm[0]) ?? { getRange: { fromRow: 0, fromCol: 0, toRow: 0, toCol: 0 } }
//   let message = new vscode.MarkdownString().appendCodeblock("Type mismatch here", "txt")
//   let useMinimalType = tc.typeLeftFull === null || tc.typeRightFull === null; 
//   let logicalTypeL = useMinimalType? tc.typeLeftMinimal : tc.typeLeftFull
//   let logicalTypeR = useMinimalType? tc.typeRightMinimal : tc.typeRightFull

//   message.appendMarkdown("Possible 1")
//   message.appendCodeblock(logicalTypeL,"haskell")
//   message.appendMarkdown("Possible 2")
//   message.appendCodeblock(logicalTypeR,"haskell")
//   let ranges = new vscode.Range(place.getRange.fromRow, place.getRange.fromCol, place.getRange.toRow, place.getRange.toCol)
//   e.setDecorations(errorDeco, [{range: ranges, hoverMessage: message}]);

// }


function arrayEq(a: number[], b: number[]): boolean {
  let cloneA = R.clone(a);
  let cloneB = R.clone(b);
  cloneA.sort();
  cloneB.sort();
  return R.equals(a, b)
}

function arrayIso(pt: Place[], a: number[], b: number[]) : boolean {
  let cloneA = R.clone(a);
  let cloneB = R.clone(b);
  cloneA.sort();
  cloneB.sort();
  return R.equals(a.map(n => pt.find(p => p.getLoc == n)?.getRange), b.map(n => pt.find(p => p.getLoc == n)?.getRange))
}