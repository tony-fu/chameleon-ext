interface Place {
    getLoc: number,
    getRange: ChameleonRange
}

interface ChameleonRange {
    fromRow: number,
    tag: string,
    toCol: number,
    toRow: number,
    fromCol: number
}

interface Decl {
    declInfo: DeclInfo,
    typeConflict: TypeConflict | null,
    mismatch: Mismatch | null,
    snapshotSize: [number, number, number, number] | null,
    chain: ChameleonRange[][] | null,
    chainLocs : number[][] | null
}

interface DeclInfo {
    declaredLoc: number,
    declaredFile: string,
    nameOriginal: string,
    nameQualified: string,

}

interface TypeConflict {
    conflictTerm: number[],
    typeLeftMinimal: string,
    typeLeftFull: string,
    typeRightMinimal: string,
    typeRightFull: string,
    constraints: Constraint[],
}

interface Mismatch {
    allConstraints: Constraint[],
    typeVariants: TypeVariant[],
}

interface TypeVariant {
    signature: string,
    locs: number[]
}

interface Constraint {
    just: number[],
    left: Type,
    right: Type
}

type Type = Var | Con | App;

interface Var {
    class: "var",
    name: string
}

interface Con {
    class: "con",
    name: string
}

interface App {
    class: "app",
    type1: Type,
    type2: Type
}

interface ChameleonResult {
    placetable: Place[],
    filepath: string,
    decls: Decl[],
}