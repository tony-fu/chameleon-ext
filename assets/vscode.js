const vscode = acquireVsCodeApi();
LogRocket.init(
    'fqysam/chameleon', 
    {
        mergeIframes: true,
        dom: {
            baseHref: 'https://open-box.s3.ap-southeast-2.amazonaws.com/',
        },
    });

document.querySelectorAll('.type-variant')
    .forEach(block => {
        block.addEventListener('mouseenter', event => {
            vscode.postMessage({
                command: 'highlight',
                locations: event.target.dataset.loc.split(',').map(n => parseInt(n, 10)),
                fcolor: event.target.dataset.fcolor,
                bcolor: event.target.dataset.bcolor,
            })
        })

        block.addEventListener('mouseleave', _ => {
            vscode.postMessage({
                command: 'refresh'
            })
        })
    })

document.querySelectorAll('.deduction-grid')
    .forEach(block => {
        block.addEventListener('mouseenter', event => {
            document.querySelectorAll('.deduction-grid').forEach(b => {
                b.classList.remove('active')
            })
            block.classList.add('active')
            vscode.postMessage({
                command: 'highlight',
                locations: event.target.dataset.loc.split(',').map(n => parseInt(n, 10)),
                fcolor: event.target.dataset.fcolor,
                bcolor: event.target.dataset.bcolor,
            })
        })

        block.addEventListener('mouseleave', _ => {
            document.querySelectorAll('.deduction-grid').forEach(b => {
                b.classList.remove('active')
            })
            vscode.postMessage({
                command: 'refresh'
            })
        })
    })